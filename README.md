## Installation

```
npm i
```

## Starting the server

### Local environment variables

```typescript
// .env
// starting port
PORT!: number;
NODE_ENV: "development" | "production";
// the access link to the mongo db including username and password
DATABASE_ACCESS_URL!: string;
// secret to generate access token
JWT_PRIVATE_KEY!: string;
```

```
npm start
```

## deployment

Pushing to the main branch will trigger deployment
