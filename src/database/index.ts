import mongoose from "mongoose";
import logger from "../utils/logger";

export const connectToDB = async (callback: Function) => {
    try {
        mongoose.set("strictQuery", true);
        await mongoose.connect(process.env.DATABASE_ACCESS_URL as string, {
            dbName: "on-live-it",
        });
        logger.info("Connected to database");
        callback();
    } catch (error: any) {
        logger.error("DB connection error: ", error.message);
    }
};
