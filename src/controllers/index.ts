import { Request, Response } from "express-serve-static-core";

export const getServerStatus = (_: Request, res: Response) => {
    return res.status(200).json({ serverUp: true });
};
