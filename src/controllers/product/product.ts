import { FileArray, UploadedFile } from "express-fileupload";
import { Request, Response } from "express-serve-static-core";
import sharp from "sharp";

import Product, { IProduct } from "../../models/product.model.";
import { AuthenticatedRequest } from "../../types/auth";
import { handleSchemaShapeError } from "../../utils/errorHandling";
import { validateImage } from "../../utils/image";
import logger from "../../utils/logger";

export const getProducts = async (req: Request, res: Response) => {
    try {
        const products = await Product.find({});

        const mappedProducts = products.map((product) => ({
            productId: product._id,
            cost: product.cost,
            title: product.title,
        }));

        logger.info(`Products sent: ${mappedProducts.map((product) => product.productId).toString()}`);
        return res.json({ products: mappedProducts });
    } catch (error: any) {
        logger.error(error.message);
        return res.status(500).json({ message: error.message });
    }
};

export const addProduct = async (req: AuthenticatedRequest<any, any, IProduct>, res: Response) => {
    try {
        const body = req.body;

        const product = new Product({
            title: body.title,
            cost: body.cost,
            active: body.active,
        });

        const err = handleSchemaShapeError(product);

        if (err) {
            logger.warn(err.message);
            return res.status(400).json({
                message: err.message,
            });
        }

        await product.save();

        logger.info(`add product: ${JSON.stringify(product)}`);
        return res.json({ productId: product._id });
    } catch (error: any) {
        logger.error(`Failed to save Product: ${error.message}`);
        return res.status(500).json({ message: error.message });
    }
};

export const deleteProduct = async (productId: string) => {
    try {
        logger.info("Remove product without image.");
        await Product.deleteOne({ _id: productId });
    } catch (error: any) {
        logger.error(error.message);
        throw new Error("Product not found.");
    }
};

export const uploadProductImage = async (req: Request<{ productId: string }>, res: Response) => {
    try {
        const errorMessage = validateImage(req.files);
        if (errorMessage) {
            await deleteProduct(req.params.productId);
            logger.warn(errorMessage);
            return res.status(400).json({ message: errorMessage });
        }
        const file: UploadedFile = (req.files as FileArray).file as UploadedFile;

        const image = await sharp(file.data).resize(286, 214, { fit: "cover" }).toFormat("jpg").toBuffer();
        const base64Image = image.toString("base64");

        const product = await Product.findOneAndUpdate(
            { _id: req.params.productId },
            {
                image: base64Image,
            },
            { new: false }
        );

        if (!product) {
            logger.warn("Product not found.");
            return res.status(404).json({ message: "Product not found." });
        }

        const schemaError = handleSchemaShapeError(product);
        if (schemaError) {
            logger.warn(schemaError.message);
            return res.status(400).json({
                message: schemaError.message,
            });
        }

        logger.info(`Product image successfully added with id: ${req.params.productId}`);
        return res.json({ message: "success" });
    } catch (error: any) {
        await deleteProduct(req.params.productId);
        logger.error(error.message);
        return res.status(500).json({ message: error.message });
    }
};
/**
 * serve product images directly from the database
 */
export const getProductImage = async (req: Request<{ productId: string }>, res: Response) => {
    try {
        const product = await Product.findOne({ _id: req.params.productId });
        if (!product) {
            logger.warn(`Product not found with id: ${req.params.productId}`);
            return res.status(404).json({ message: "Product not found." });
        }

        const buffer = Buffer.from(product.image, "base64");

        res.writeHead(200, {
            "Content-Type": "image/jpeg",
            "Content-Length": buffer.length,
        });

        logger.info(`Successful product image retrieved with id: ${req.params.productId}`);
        return res.end(buffer);
    } catch (error: any) {
        logger.error(error.message);
        return res.status(500).json({ message: error.message });
    }
};
