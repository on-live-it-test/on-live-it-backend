import { Request, Response } from "express-serve-static-core";
import { Error } from "mongoose";
import User, { IUser } from "../../models/user.model";
import logger from "../../utils/logger";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

export const registerUser = async (req: Request<{}, {}, IUser>, res: Response) => {
    try {
        const body: IUser = req.body;

        const existingUser = await User.findOne({ email: body.email });

        if (existingUser) {
            logger.warn({ message: `User already exists with email: ${req.body.email}` });
            return res.status(409).json({ message: "User already exists!" });
        }

        const hashedPassword = await bcrypt.hash(body.password, 12);

        const user = new User({
            password: hashedPassword,
            email: body.email,
        });

        const err = user.validateSync();

        if (err instanceof Error.ValidationError) {
            logger.warn(err.message);
            return res.status(400).json({
                message: err.message,
            });
        }

        await user.save();
        logger.info(`User created: ${body.email} ${hashedPassword}`);
        return res.status(200).json({ message: "success" });
    } catch (error: any) {
        logger.error(`failed to save user ${(error as Error).message}`);
        res.status(500).json({ message: `Failed to save user: ${(error as Error).message}` });
    }
};

//TODO: handle refresh token
export const loginUser = async (req: Request<{}, {}, IUser>, res: Response) => {
    try {
        const body: IUser = req.body;

        logger.info(`login body: ${JSON.stringify(body)}`);

        const user = await User.findOne({ email: body.email });

        if (user) {
            const passwordIsValid = await bcrypt.compare(body.password, user.password);

            if (passwordIsValid) {
                const { email, id } = user;
                const token = jwt.sign(
                    {
                        email: email,
                        userId: id,
                    },
                    process.env.JWT_PRIVATE_KEY as string,
                    {
                        expiresIn: "1h",
                    }
                );

                logger.info(`User logged in: ${email}`);
                return res.status(200).json({ token: `Bearer ${token}`, expiresIn: 3600 });
            }
        }

        logger.warn(`Invalid password for user: ${body.email}`);
        return res.status(401).json({ message: "Authentication failed." });
    } catch (error) {
        logger.error(`Auth unsuccessful: ${(error as Error).message}`);
        return res.status(401).json({ message: "Authentication failed." });
    }
};
