import express from "express";
import { config } from "dotenv";
import fileUpload from "express-fileupload";
import cors from "cors";

import { connectToDB } from "./database";
import morganMiddleware from "./middlewares/morganMiddleware";
import logger from "./utils/logger";
import routes from "./routes";

config();

const PORT = process.env.PORT ?? 8000;
const app = express();

app.use(
    fileUpload({
        createParentPath: true,
        limits: {
            fileSize: 3000000, // Around 3MB
        },
        abortOnLimit: true,
    })
);

const origins =
    process.env.NODE_ENV === "development" ? ["https://onlive-frontend.onrender.com", "localhost"] : ["https://onlive-frontend.onrender.com"];

app.use(cors({ origin: origins, optionsSuccessStatus: 200 }));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use("/api", routes);

app.use(morganMiddleware);

connectToDB(() => {
    app.listen(PORT, async () => {
        logger.info(`Server started on ${PORT}`);
    });
});

export default app;
