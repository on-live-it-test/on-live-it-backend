import { Request, Response, NextFunction } from "express-serve-static-core";
import { verify } from "jsonwebtoken";
import { AuthenticatedRequest } from "../types/auth";
import logger from "../utils/logger";

/**
 * @desc Guards the route from unauthorized users by checking the authorization jwt token
 */
const authMiddleware = (req: Request, res: Response, next: NextFunction) => {
    try {
        const token = (req.headers.authorization as string)?.split(" ")[1];
        const decodedToken = verify(token, process.env.JWT_PRIVATE_KEY as string);

        (req as AuthenticatedRequest<any, any, typeof req.body>["body"]).token = decodedToken;
        next();
    } catch (error: any) {
        logger.error(`Token verification failed: error: ${error.message}`);
        return res.status(401).json({ message: error.message });
    }
};

export default authMiddleware;
