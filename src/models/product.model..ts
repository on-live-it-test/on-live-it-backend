import mongoose, { Schema } from "mongoose";

export interface IProduct {
    title: string;
    cost: number;
    active: boolean;
    image: string;
}

const productSchema = new Schema<IProduct>({
    title: {
        type: String,
        required: true,
    },
    cost: {
        type: Number,
        required: true,
    },
    active: {
        type: Boolean,
        required: true,
    },
    image: {
        type: String,
        require: true,
    },
});

const Product = mongoose.model<IProduct>("Product", productSchema);

export default Product;
