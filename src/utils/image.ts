import { FileArray, UploadedFile } from "express-fileupload";

import logger from "./logger";

export const isFileValid = (files: FileArray | undefined | null): boolean => {
    if (!files || Object.keys(files).length < 1) return false;
    return !!files.file;
};

export const isFileAnImage = (image: UploadedFile) => {
    const isImage = /^image/.test(image.mimetype);

    if (!isImage) {
        logger.error("Uploaded file not an image");
    }

    return isImage;
};

const allowedImgExtensions = ["png", "jpg", "svg", "jpeg"];

export const isValidImageFormat = (image: UploadedFile) => {
    const isAllowedImageType = allowedImgExtensions.some((ext) => image.mimetype.includes(ext));

    if (!isAllowedImageType) {
        logger.error("Uploaded image type not allowed");
    }

    return isAllowedImageType;
};

export const validateImage = (files: FileArray | undefined | null) => {
    if (!files || Object.keys(files).length === 0) return "File is missing.";
    if (!isFileValid(files)) return "Invalid file.";
    const file: UploadedFile = files.file as UploadedFile;
    if (!isFileAnImage(file)) return "File is not an image.";
    if (!isValidImageFormat(file)) return "Invalid image format.";
    return null;
};
