import { Error, Document, Types } from "mongoose";
import logger from "./logger";

export const handleSchemaShapeError = <Schema>(
    schemaInstance: Document<unknown, any, Schema> &
        Schema & {
            _id: Types.ObjectId;
        }
) => {
    const err = schemaInstance.validateSync();

    if (err instanceof Error.ValidationError) {
        logger.error(err.message);

        return err;
    }

    return null;
};
