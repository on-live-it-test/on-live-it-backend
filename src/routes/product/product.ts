import { Router } from "express";

import { addProduct, getProductImage, getProducts, uploadProductImage } from "../../controllers/product/product";
import authMiddleware from "../../middlewares/auth";
import fileAuthMiddleware from "../../middlewares/fileAuth";

const router = Router();

router.get("/", getProducts);

router.post("/add", authMiddleware, addProduct);

router.post("/images/upload/:productId", fileAuthMiddleware, uploadProductImage);

router.get("/images/:productId", getProductImage);

export default router;
