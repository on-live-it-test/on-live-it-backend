import { Router } from "express";
import authRouter from "./auth/auth";
import productsRouter from "./product/product";
import { getServerStatus } from "../controllers";

const router = Router();

router.get("/", getServerStatus);

router.use("/auth", authRouter);
router.use("/products", productsRouter);

export default router;
