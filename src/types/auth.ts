import { Request, ParamsDictionary, Query } from "express-serve-static-core";

export type AuthenticatedBody<ReqBody> = {
    token: {
        email: string;
        userId: string;
    };
} & ReqBody;

export type AuthenticatedRequest<P = ParamsDictionary, ResBody = any, ReqBody = any, ReqQuery = Query> = Request<
    P,
    ResBody,
    AuthenticatedBody<ReqBody>,
    ReqQuery
>;
