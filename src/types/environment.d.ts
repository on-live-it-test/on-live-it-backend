export {};

declare global {
    namespace NodeJS {
        interface ProcessEnv {
            PORT: number;
            NODE_ENV: "development" | "production";
            DATABASE_ACCESS_URL: string;
            JWT_PRIVATE_KEY: string;
        }
    }
}
